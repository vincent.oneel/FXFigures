package com.vminakov.figuresfx.controller;

import com.vminakov.figuresfx.figures.Circle;
import com.vminakov.figuresfx.figures.Figure;
import com.vminakov.figuresfx.figures.Rectangle;
import com.vminakov.figuresfx.figures.Triangle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;

public class MainScreenController implements Initializable {

    private Figure[] figures;
    private Random random;

    @FXML
    private Canvas canvas;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        figures = new Figure[1];
        random = new Random(System.currentTimeMillis());
    }

    @FXML
    private void onMouseClicked(MouseEvent evt) {
        addFigure(createFigure(evt.getX(), evt.getY()));
        repaint();
    }

    private void repaint(){
        canvas.getGraphicsContext2D().clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        for (Figure figure : figures) {
            if (figure != null){
                figure.draw(canvas.getGraphicsContext2D());
            }
        }
    }

    private Figure createFigure(double x, double y){
        Figure figure = null;
        switch (random.nextInt(3)){
            case Figure.FUGURE_TYPE_CIRCLE:
                figure = new Circle(x, y, random.nextInt(4), Color.CORNFLOWERBLUE, random.nextInt(50));
                break;
            case Figure.FUGURE_TYPE_RECT:
                figure = new Rectangle(x, y, random.nextInt(4), Color.DEEPPINK, random.nextInt(100), random.nextInt(100));
                break;
            case Figure.FUGURE_TYPE_TRIANGLE:
                figure = new Triangle(x, y, random.nextInt(4), Color.FUCHSIA, random.nextInt(100));
                break;
            default:
                System.out.println("unknown figure type");
        }

        return figure;
    }

    private void addFigure(Figure figure){
        if (figures[figures.length - 1] == null){
            figures[figures.length - 1] = figure;
            return;
        }

        Figure[] tmp = new Figure[figures.length + 1];
        int index = 0;
        for ( ; index < figures.length; index++){
            tmp[index] = figures[index];
        }
        tmp[index] = figure;
        figures = tmp;
    }
}
